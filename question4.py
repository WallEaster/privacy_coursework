#!/usr/bin/env python

import numpy as np
import re


def get_input_and_output_size():
    a, b = map(int, raw_input("Enter: [Num inputs] [Num outputs] \n").split())
    return a, b


def encrypt_not(e, p, w, c, circuit):
    c += 1
    e = np.append(e, not_gate(p[int(circuit[c])], p[w]))
    w += 1
    return e, c, w


def encrypt_and(e, p, w, c, circuit, x):
    c += 1
    if circuit[c] == "~":
        e, c, w = encrypt_not(e, p, w, c, circuit)
        e = np.append(e, and_gate(p[x], p[w - 1], p[w]))
    else:
        e = np.append(e, and_gate(p[x], p[int(circuit[c])], p[w]))

    w += 1
    if c < len(circuit) - 1:
        c += 1
        if circuit[c] == ".":
            e, c, w = encrypt_and(e, p, w, c, circuit, w - 1)

    return e, c, w


def encrypt_or(e, p, w, c, circuit, x):
    # pointing at either 'number' of 'not'
    c += 1
    hasNot = False
    if circuit[c] == "~":
        e, c, w = encrypt_not(e, p, w, c, circuit)
        hasNot = True

    # pointing at operator after 'number' or beyond circuit
    c += 1
    if c < len(circuit):
        if circuit[c] == ".":
            if hasNot:
                e, c, w = encrypt_and(e, p, w, c, circuit, w - 1)
            else:
                e, c, w = encrypt_and(e, p, w, c, circuit, int(circuit[c - 1]))

        e = np.append(e, or_gate(p[x], p[w - 1], p[w]))
    else:
        if hasNot:
            e = np.append(e, or_gate(p[x], p[w - 1], p[w]))
        else:
            e = np.append(e, or_gate(p[x], p[int(circuit[c - 1])], p[w]))

    # pointing at next 'or' operator

    w += 1

    return e, c, w


def build_encryption(circuit, numI):
    g = len(re.findall("~|\.|\+|\^", circuit))
    # generate random keys
    p = np.random.randint(2, size=g + numI)
    # p = np.zeros(g+numI, int)
    e = np.array([], np.int8)
    w = numI
    c = 0
    while c < (len(circuit)):
        if circuit[c] == "~":
            e, c, w = encrypt_not(e, p, w, c, circuit)
        elif circuit[c] == ".":
            x = w - 1
            if w == numI:
                x = int(circuit[c - 1])
            e, c, w = encrypt_and(e, p, w, c, circuit, x)
        elif circuit[c] == "+":
            x = w - 1
            if w == numI:
                x = int(circuit[c - 1])
            e, c, w = encrypt_or(e, p, w, c, circuit, x)
        elif circuit[c] == "^":
            x = w - 1
            if w == numI:
                x = int(circuit[c - 1])
            c += 1
            if circuit[c] == "~":
                e, c, w = encrypt_not(e, p, w, c, circuit)
                e = np.append(e, xor_gate(p[x], p[w - 1], p[w]))
            else:
                e = np.append(e, xor_gate(p[x], p[int(circuit[c])], p[w]))
            w += 1
        else:
            c += 1
            continue

    return e, p[w - 1], p[0:numI]


def decrypt_conjunction(ec, c, ei, ee, kx, ik, input, circuit):
    c += 1
    if circuit[c] == "~":
        ee += 6
        c += 1
        if ec[ei + (input[int(circuit[c])] ^ ik[int(circuit[c])])] == 1:
            ei += 3
        else:
            ei += 2
    else:
        ee += 4
        ei += input[int(circuit[c])] ^ ik[int(circuit[c])]

    ei += 2 * kx

    kx = ec[ei]
    ei = ee
    c += 1
    if c < len(circuit):
        if circuit[c] == "." or circuit[c] == "^":
            c, ei, ee, kx = decrypt_conjunction(ec, c, ei, ee, kx, ik, input, circuit)

    return c, ei, ee, kx


def decrypt_disjunction(ec, c, ei, ee, kx, ik, input, circuit):
    c += 1

    if circuit[c] == "~":
        c += 1
        ei += (input[int(circuit[c])] ^ ik[int(circuit[c])])
        ee += 2
        kxx = ec[ei]
        ei = ee
    else:
        kxx = input[int(circuit[c])] ^ ik[int(circuit[c])]

    c += 1
    if c < len(circuit):
        if circuit[c] == "." or circuit[c] == "^":
            c, ei, ee, kxx = decrypt_conjunction(ec, c, ei, ee, kxx, ik, input, circuit)

    ee += 4
    ei += kxx
    ei += 2 * kx

    kx = ec[ei]
    ei = ee

    return c, ei, ee, kx


def decrypt(ec, k, circuit, ik, input):
    c = 0
    ei = 0
    ee = 0
    kx = 0
    while c < (len(circuit)):
        if circuit[c] == "~":
            c += 1
            ei += (input[int(circuit[c])] ^ ik[int(circuit[c])])
            ee += 2
            kx = ec[ei]
            ei = ee
            c += 1
        elif circuit[c] == "+":
            c, ei, ee, kx = decrypt_disjunction(ec, c, ei, ee, kx, ik, input, circuit)
        elif circuit[c] == "." or circuit[c] == "^":
            c, ei, ee, kx = decrypt_conjunction(ec, c, ei, ee, kx, ik, input, circuit)
        else:
            kx = (input[int(circuit[c])] ^ ik[int(circuit[c])])
            c += 1

    return kx ^ k


def and_gate(x, y, z):
    return [z ^ ((0 ^ x) & (0 ^ y)), z ^ ((0 ^ x) & (1 ^ y)), z ^ ((1 ^ x) & (0 ^ y)), z ^ ((1 ^ x) & (1 ^ y))]


def or_gate(x, y, z):
    return [z ^ ((0 ^ x) | (0 ^ y)), z ^ ((0 ^ x) | (1 ^ y)), z ^ ((1 ^ x) | (0 ^ y)), z ^ ((1 ^ x) | (1 ^ y))]


def xor_gate(x, y, z):
    return [z ^ ((0 ^ x) ^ (0 ^ y)), z ^ ((0 ^ x) ^ (1 ^ y)), z ^ ((1 ^ x) ^ (0 ^ y)), z ^ ((1 ^ x) ^ (1 ^ y))]


def not_gate(x, z):
    return [z ^ ((0 ^ x) ^ 1), z ^ ((1 ^ x) ^ 1)]


if __name__ == "__main__":
    numI, numO = get_input_and_output_size()

    outputCircuits = []

    for i in range(0, numO):
        outputCircuits.append(raw_input("Please provide the boolean expression for output bit: " + repr(
            i) + ".\nOperators supported are: '~', '.', '+', '^' for 'not', 'and', 'or', 'xor' respectively." +
                                        " Input wires are numbered from 0 and formulae must not have spaces. (e.g. 0.2+1.~3)\n"))

    encryption = []
    finalKeys = []
    inputKeys = []

    for i in range(0, numO):
        # for oblivious transfer there would be two keys per input;
        # in my case these two keys are identical which breaks the secure protocol
        ec, k, ik, = build_encryption(outputCircuits[i], numI)
        encryption.append(ec)
        finalKeys.append(k)
        # Instead of simply giving the decrypter the keys, the keys for the input provided by the other party will
        # need to be sent by oblivious transfer so that the encrypter will not learn which key was used
        inputKeys.append(ik)
        print "The garbled circuit for output[" + repr(i) + "] is ", encryption[
            i], "; Final key = ", k, "; Keys for input wires = ", ik

    while True:
        input = map(int, raw_input(
            "To calculate an output, please enter " + repr(
                numI) + " space separated [0,1] values corresponding to each input bit. \n" + "Or hit enter to exit. \n").split())
        if not input:
            break

        output = []
        for i in range(0, numO):
            output.append(decrypt(encryption[i], finalKeys[i], outputCircuits[i], inputKeys[i], input))
        print input, "->", output
