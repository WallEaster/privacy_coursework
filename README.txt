The python file question4.py contains the full implementation for the garbled circuits as specified in question 4 of the
 coursework. The implementation is in python2.7.
The program takes command line inputs to first encrypt a boolean expression as a garbled circuit, followed by sets of
input bits which it runs through the garbled circuit and returns the decrypted output.
The output for all 16 input configurations can be found in the question5.txt file. This is also generated when running
the cmp.sh script.

There are two bash scripts provided for running the python program:

cmp.sh - This script runs the python program with the circuit based on the compare function specified in Q1. It also
enters all 16 sets of input configurations.

interactive.sh - This script simply invokes the python script using shell commands. This begins an interactive session

To run from shell terminal,

chmod +x {script name}
./{script name}

alternatively, you can run the python script directly:

python question4.py

